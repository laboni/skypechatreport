import os
import json
from datetime import datetime
import pandas as pd


# datetime string to date object
def try_parsing_date(text):
    for fmt in ("%Y-%m-%dT%H:%M:%S.%fZ", "%Y-%m-%dT%H:%M:%SZ", "%Y-%m-%d %H:%M:%S", "%Y-%m-%d"):
        try:
            return datetime.strptime(text, fmt)
        except ValueError:
            pass
    raise ValueError("no valid date format found")

# model class
class Sheet:
  def __init__(self, name = ""):
    self.name = name
    self.pivot_table = []

# configurations
skype_json_file_location = "/home/laboni/Projects/my_online_repo/gitlab/skypechatreport/skype-messages.json"
spreadsheet_location = "/home/laboni/Projects/my_online_repo/gitlab/skypechatreport/result.xlsx"
start_date = try_parsing_date("2020-04-01 00:00:00") # "2020-04-01"
end_date = try_parsing_date("2020-04-30 23:59:59")   # "9999-12-31"

# read json file and create report data
skype_json = json.loads(open(skype_json_file_location).read())

# create report data
sheet_list = []
for group in skype_json:
  messages = group["messages"]
  for message in messages :
    message["Name"] = message["displayName"]
    date_obj = try_parsing_date(message["originalarrivaltime"])
    message["DateObj"] = date_obj
    message["Date"] = date_obj.strftime("%Y-%m-%d")

  filted_messages = [x for x in messages if (start_date <=  x['DateObj'] and x['DateObj'] <= end_date)]
  if len(filted_messages) != 0:
    sheet = Sheet(group["group"])
    sheet.pivot_table = pd.DataFrame(filted_messages).groupby(["Date", "Name"])["content"].count().reset_index(name="Count").pivot("Name", "Date").fillna("")
    sheet_list.append(sheet)

# file export
if os.path.exists(spreadsheet_location):
  os.remove(spreadsheet_location)
writer = pd.ExcelWriter(spreadsheet_location)
for sheet in sheet_list:
  sheet.pivot_table.to_excel(writer, sheet.name)
writer.save()